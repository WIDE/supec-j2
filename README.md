# SUPEC-J2 delivrable

Semestre Supervision de systèmes dynamiques : techniques émergentes pour l’investigation d’activités anormales (SUPSEC). 

Link to the main supsec webpage [here](https://semestres-cyber.inria.fr/supsec/).

## Links to events of the semester and presented slides

[Security Monitoring](https://www.irisa.fr/en/date/2022-02/workshop-security-monitoring-supsec-march-15-16-inria-rennes) - [slides](https://gitlab.inria.fr/WIDE/supec-j2/-/tree/main/slides/1)

[Supervision de sécurité et gestion de données massives](https://supsec-workshop-summer2022.inria.fr/programme/) - [slides](https://gitlab.inria.fr/WIDE/supec-j2/-/tree/main/slides/2)

[AI for supervision](https://supsec.github.io/) - [slides](https://gitlab.inria.fr/WIDE/supec-j2/-/tree/main/slides/3)

[Winter Workshop](https://supsec-winter-workshop.github.io/supsec-winter-workshop/talks.html) - [slides](https://gitlab.inria.fr/WIDE/supec-j2/-/tree/main/slides/4)

##### Remarks

* Four websites have been set up, one per event of the semester.

* Not all the slides have been made available publicly, by the choice of some speakers/institutions. 

* In addition to the slides being linked on some event pages, we added below repositories for a bulk access, and for better preservation in time in the Inria IT infrastructure.

## Budget exécuté

* Convention: 120000€ TTC de montant global
    * au J2: 30% (J1) + 60% (J2) de subvention demandé à date
* Au 18/04/2023: 28 033.39€ ont été effectivement dépensés/réglés
    * Solde: 68 000€ (déduction faite du versement des 4% au BMINF)
